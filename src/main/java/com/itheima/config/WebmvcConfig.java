package com.itheima.config;

import com.itheima.interceptor.CheckLoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration //标识当前类，是一个配置类
public class WebmvcConfig implements WebMvcConfigurer {//实现WebMvcConfigurer，设置当前配置类是一个web相关的配置类

    @Autowired
    CheckLoginInterceptor checkLoginInterceptor;

    //配置拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册拦截器
        registry.addInterceptor(checkLoginInterceptor)
                //配置拦截路径
                .addPathPatterns("/**") //  拦截所有请求
                //.addPathPatterns("/*") //  拦截任意一级目录资源请求
                //.addPathPatterns("/depts/*") //  拦截任意模块任意一级目录资源请求
                //.addPathPatterns("/depts/**") //  拦截任意模块任意资源请求
                .excludePathPatterns("/login"); //不拦截的指定请求

    }
}
