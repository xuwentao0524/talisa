package com.itheima.proxy;

/**
 * 被代理对象
 */
public class HelloImpl implements Hello {
    @Override
    public void sayHello() {
        for (int i = 0; i < 10000; i++) {
            System.out.println("kunkun:"+i);
        }
    }
}
