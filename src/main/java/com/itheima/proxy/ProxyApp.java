package com.itheima.proxy;

import org.springframework.cglib.proxy.Proxy;

public class ProxyApp {

    public static void main(String[] args) {
        //被代理对象（坤哥）
        Hello hello = new HelloImpl();

        //代理对象和被代理对象具体的代理细节
        MyInvocationHandler handler = new MyInvocationHandler(hello);

        //代理对象（律师） ：JDK动态代理
        Hello proxyHello = (Hello) Proxy.newProxyInstance(
                hello.getClass().getClassLoader(),
                hello.getClass().getInterfaces(), handler);

        //目标方法
        proxyHello.sayHello();
    }
}
