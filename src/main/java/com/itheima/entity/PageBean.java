package com.itheima.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 封装分页查询结果数据：
 *      当前页的数据List
 *      当前数据表的总记录数total
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageBean {

    private Integer total;
    private List rows;

}
