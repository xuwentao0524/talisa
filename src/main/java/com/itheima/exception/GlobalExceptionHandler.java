package com.itheima.exception;

import com.itheima.entity.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//标识当前类是一个全局异常处理器
@RestControllerAdvice
public class GlobalExceptionHandler {

    //设置当前方法处理指定类型的异常
    @ExceptionHandler
    public Result  handleException(Exception e){
        System.out.println("程序发生异常了："+e.getMessage());
        return Result.error("系统异常，请联系管理员！");
    }

}
