package com.itheima.controller;


import com.itheima.entity.Emp;
import com.itheima.entity.PageBean;
import com.itheima.entity.PageQueryBean;
import com.itheima.entity.Result;
import com.itheima.service.EmpService;
import com.itheima.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpHead;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/*
    处理所有员工相关请求的处理器类：指责单一
        1. 处理前端请求
        2. 调用业务层实现具体的功能
        3. 封装处理结果
 */

@Slf4j
@RestController
@RequestMapping("/emps")
public class EmpController {

    @Autowired
    EmpService empService;

    /**
     * 分页查询员工列表
     */
    @GetMapping
    public Result select(PageQueryBean pageQueryBean) {
        PageBean pageBean = empService.select(pageQueryBean);
        return Result.success(pageBean);
    }


    /**
     * 批量删除员工
     */
    @DeleteMapping("/{ids}")
    public Result deleteBatch(@PathVariable List<Integer> ids) {

        //1. 接收请求参数：请求路径中的参数path，@PathVariable
        log.info("批量删除，ids={}", ids);
        //2. 调用业务层，实现批量删除员工
        empService.deleteBatch(ids);

        //3. 封装并返回处理结果
        return Result.success();
    }

    /**
     * Restful  :  get/delete/put/post添加
     */
    @PostMapping
    public Result add(@RequestBody Emp emp) {
        //1.接收请求参数
        log.info("添加员工：{}", emp);
        //2.调用业务层
        empService.addEmp(emp);
        //3.封装结果：code=1  msg=success
        return Result.success();
    }


    /**
     * 回显员工详情信息
     * 方法名称：queryOne
     * 请求类型：
     * 请求路径：
     * 请求参数：
     * 响应的结果：
     */
    @GetMapping("/{id}")
    public Result queryOne(@PathVariable Integer id) {
        Emp emp = empService.queryOne(id);
        return Result.success(emp);
    }


    /**
     * 更新员工
     */
    @PutMapping
    public Result update(@RequestBody Emp emp) {
        empService.update(emp);
        return Result.success();
    }


}






















