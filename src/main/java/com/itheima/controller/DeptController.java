package com.itheima.controller;


import com.itheima.entity.Dept;
import com.itheima.entity.Emp;
import com.itheima.entity.Result;
import com.itheima.mapper.EmpMapper;
import com.itheima.service.DeptService;
import com.itheima.service.impl.DeptServiceImpl;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
    处理所有部门相关请求的处理器类：指责单一
        1. 处理前端请求
        2. 调用业务层实现具体的功能
        3. 封装处理结果
 */
@RestController
@RequestMapping("/depts")
public class DeptController {
    @Autowired
    DeptService deptService;

    /**
     * 查询部门列表：请求路径/请求方式/请求参数/响应结果
     */
    @GetMapping
    public Result listDept() {

        System.out.println("3.访问目标资源... ...");

        //1. 接收请求参数:无
        //2. 调用业务层，实现部门列表查询业务功能
        List<Dept> deptList = deptService.listDept();
        //3. 封装并返回查询结果
        return Result.success(deptList); //result[code=1,msg=success,data=查询到的部门列表]
    }


    /**
     * 根据指定的id删除部门
     */
    @DeleteMapping("/{id}")
    public Result remove(@PathVariable Integer id) throws ClassNotFoundException {
        //1. 接收请求参数：请求路径，@PathVariable  +  变量

        //2. 调用业务层，实现删除指定部门的功能
        deptService.removeById(id);

        //3. 封装处理结果数据
        return Result.success(); //code = 1  msg = success  data=null
    }


    /**
     * 添加部门
     */
    @PostMapping
    public Result save(@RequestBody Dept dept) {
        //1. 接收请求参数：json格式请求参数（@RequestBody  + 实体类）

        //2. 调用业务层，实现添加部门功能
        deptService.saveDept(dept);

        //3. 封装处理结果数据
        return Result.success();//code=1,msg=success,data=null
    }


    /**
     * 根据ID查询
     */
    @GetMapping("/{id}")
    public Result queryById(@PathVariable Integer id) {
        //1. 接收请求参数
        //2. 调用业务层实现具体的功能
        Dept dept = deptService.queryDeptById(id);
        //3. 封装并返回处理结果
        return Result.success(dept);//code=1 ,msg=success ,data=查询到的部门信息
    }

    @PutMapping
    public Result update(@RequestBody Dept dept) {
        deptService.update(dept);
        return Result.success();
    }


}
