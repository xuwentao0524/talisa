package com.itheima.controller;

import com.itheima.entity.Emp;
import com.itheima.entity.Result;
import com.itheima.service.EmpService;
import com.itheima.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class LoginController {

    @Autowired
    EmpService empService;

    @PostMapping("/login")
    public Result login(@RequestBody Emp emp){
        //1. 调用业务层，实现员工登录功能
        Emp empResult  = empService.checkLogin(emp);

        //2. 判断用户是否登录成功
        //2.1登录失败
        if(empResult==null){
            return Result.error("登录失败，please open  your big  eyes,check your info");
        }
        //2.2 登录成功，颁发JWT令牌（用户身份标识：字符串）
        //设置载荷信息
        Map claims = new HashMap();
        claims.put("empId",empResult.getId());
        claims.put("username",empResult.getUsername());

        //调用工具类，生成令牌
        String jwtTokenStr = JwtUtils.generateJwt(claims);

        //3. 封装/返回处理结果
        return Result.success(jwtTokenStr); //code= 1 ,msg=success  ,data=jwt令牌
    }

}
