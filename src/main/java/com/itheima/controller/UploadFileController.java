package com.itheima.controller;

import com.itheima.entity.Result;
import com.itheima.util.FileUploadUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Slf4j
@RestController
public class UploadFileController {

    @Autowired
    FileUploadUtils fileUploadUtils;

    /**
     * 处理文件上传请求，存储到oss
     */
    @PostMapping("/upload")
    public Result upload(MultipartFile image) throws IOException {
        log.info("处理表单提交");
        //调用工具类，实现文件上传
        String path = fileUploadUtils.uploadFile(image);

        return Result.success(path);
    }
}
