package com.itheima.interceptor;

import com.itheima.util.JwtUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录验证拦截器
 */
@Component
public class CheckLoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1. 访问其他的资源（直接想进入小区），验证用户的身份
        String token = request.getHeader("Token");
        //1.1 检查令牌（出示门禁卡）
        if(token==null){
            //没有携带令牌（没有小区的门禁卡），禁止后续访问
            //跳转登录页面（办理门禁的办事处，验证身份办理门禁）
            //手动封装响应结果数据：{"code":0,"msg":"NOT_LOGIN"}
            response.getWriter().write("{\"code\":0,\"msg\":\"NOT_LOGIN\"}");
            return false; //注意：
        }
        //3.2 校验jwt令牌是否正确
        try {
            JwtUtils.parseJWT(token);
        } catch (Exception e) {
            e.printStackTrace();
            //如果令牌解析错误（门禁卡无效），禁止后续访问
            //跳转登录页面（办理门禁的办事处，验证身份办理门禁）
            //手动封装响应结果数据：{"code"=0,"msg"="NOT_LOGIN"}
            response.getWriter().write("{\"code\":0,\"msg\":\"NOT_LOGIN\"}");
            return false; //注意：
        }
        return true;
    }
}
