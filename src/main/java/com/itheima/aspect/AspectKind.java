package com.itheima.aspect;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 切面类
 */
@Component
//@Aspect
public class AspectKind {

    //对切入点表达式进行提取
    @Pointcut("execution(* com.itheima.service.*.*(..))")
    public void  mypt(){}



    //通知类型：前置通知
    @Before("mypt()") //切入点表达式
    //通知方法
    public void before(){
        System.out.println("前置通知... ...");
    }

    //通知类型：后置通知（发生异常执行）
    @After("mypt()") //切入点表达式
    //通知方法
    public void after(){
        System.out.println("后置通知... ...");
    }

    //通知类型：返回后通知（发生异常不执行）
    @AfterReturning("mypt()") //切入点表达式
    //通知方法
    public void AfterReturning(){
        System.out.println("返回后通知... ...");
    }


    //通知类型：异常后通知（发生异常才执行）
    @AfterThrowing("mypt()") //切入点表达式
    //通知方法
    public void AfterThrowing(){
        System.out.println("异常后通知... ...");
    }

    //通知类型：环绕通知（自主控制何时进行增强）
    @Around("mypt()") //切入点表达式
    //通知方法
    public Object Around(ProceedingJoinPoint pjo){
        Object proceed = null;
        try {
            System.out.println("环绕--前置通知... ...");
            proceed = pjo.proceed();//执行目标方法
            System.out.println("环绕--返回后通知... ...");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            System.out.println("环绕--异常后通知... ...");
        } finally {
            System.out.println("环绕--后置通知... ...");
        }

        return proceed;
    }
}
