package com.itheima.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
//@Aspect
//@Order(2) //设置通知的顺序，order的值越小，前置先执行，后置相反
public class MyAspect1 implements Ordered {

    @Pointcut("execution(* com.itheima.service.*.*(..))")
    public void pt(){}

    @Before("pt()")
    public void before(){
        System.out.println("MyAspect1-before...");
    }

    @After("pt()")
    public void after(){
        System.out.println("MyAspect1-after...");
    }

    @Override
    public int getOrder() {
        return 6;
    }
}
