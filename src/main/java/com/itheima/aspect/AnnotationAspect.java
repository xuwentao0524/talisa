package com.itheima.aspect;

import com.itheima.entity.Dept;
import com.sun.javafx.print.PrinterJobImpl;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
//@Aspect
public class AnnotationAspect {


    //通知方法:execution 指定扫描的包路径   +    annotation扫描指定的注解
    @Around("@annotation(com.itheima.annotation.MyLog) && execution(* com.itheima.service.*.*(..))")
    public Object before(ProceedingJoinPoint pjp) throws Throwable {

        Object[] args = pjp.getArgs();
        Class<?> aClass = pjp.getTarget().getClass();
        String name = pjp.getSignature().getName();
        Object proceed = pjp.proceed();

        System.out.println("环绕通知... ...");
        System.out.println("环绕通知... ...："+args);
        System.out.println("环绕通知... ..."+aClass.getName());
        System.out.println("环绕通知... ..."+name);
        System.out.println("环绕通知... ..."+(Dept)proceed);



        return proceed;
    }

}
