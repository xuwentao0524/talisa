package com.itheima.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
//@Aspect
@Order(1)
public class MyAspect2 {


    @Before("com.itheima.aspect.MyAspect1.pt()")
    public void before(){
        System.out.println("MyAspect2-before...");
    }

    @After("com.itheima.aspect.MyAspect1.pt()")
    public void after(){
        System.out.println("MyAspect2-after...");
    }
}
