package com.itheima.aspect;

import com.itheima.entity.OperateLog;
import com.itheima.mapper.OperateLogMapper;
import com.itheima.util.JwtUtils;
import io.jsonwebtoken.Claims;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.sound.midi.SysexMessage;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

@Component
@Aspect
public class OperationLogAspect {


    @Autowired
    OperateLogMapper operateLogMapper;

    @Autowired
    HttpServletRequest request;


    @Around("@annotation(com.itheima.annotation.MyLog)")
    public Object recordOperationlog(ProceedingJoinPoint pjp) throws Throwable {

        long start = System.currentTimeMillis();
        Object result = pjp.proceed();
        long end = System.currentTimeMillis();

        //向操作日志表中添加一条操作日志数据
        OperateLog operateLog = new OperateLog();
        operateLog.setOperateTime(LocalDateTime.now());
        operateLog.setClassName(pjp.getTarget().getClass().getName());
        operateLog.setCostTime(end-start);
        operateLog.setMethodName(pjp.getSignature().getName());
        operateLog.setMethodParams(Arrays.toString(pjp.getArgs()));
        operateLog.setReturnValue(result+"");


        //获取令牌
        String token = request.getHeader("Token");
        Claims claims = JwtUtils.parseJWT(token);
        operateLog.setOperateUser((Integer) claims.get("empId"));



        //核心操作
        operateLogMapper.insert(operateLog);

        return result;
    }


}
