package com.itheima.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
//@Aspect
public class ExcutionAspect {


    //完成的写法：访问修饰符/异常  通常省略不写
    //@Before("execution(public java.util.List com.itheima.service.DeptService.listDept()) throws NullpointerException")
    //使用通配符* : 任意返回值   任意包名  任意类名称  任意的方法名称    任意类型的一个参数   ，包/类/方法的任意一部分
    //@Before("execution( * com.itheima.service.impl.*ServiceImpl.sel*(*)) ")

    //使用通配符.. : 任意级别的包，任意类型/个数的参数
    @Before("execution(* *..DeptServiceImpl.listDept(..)) || execution(* *..*ServiceImpl.select(..))")


    public  void before(){
        System.out.println("前置通知... ...");
    }


}
