package com.itheima.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component  //交给ioc容器管理
//@Aspect  //标识当前类是一个切面类：定义模板方法，增强目标方法
public class TimeAspect {

    //表达式：定位需要增强的目标方法，标识对所有模块中的所有业务方法进行增强（计算耗时）
    @Around("execution(* com.itheima.service.*.*(..))")
    //定义模板方法：1.编写公共的代码/增强的代码   2.获取/执行目标方法
    public Object  countTime(ProceedingJoinPoint pjp) throws Throwable {
        long begin = System.currentTimeMillis();

        Object result = pjp.proceed();//调用目标方法，获取返回结果，并返回此返回结果

        long end = System.currentTimeMillis();

        System.out.println("执行耗时:"+(end-begin));

        return result;
    }

}
