package com.itheima.mapper;

import com.itheima.entity.Emp;
import com.itheima.entity.PageBean;
import com.itheima.entity.PageQueryBean;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 定义了所有员工相关的持久层方法
 */
@Mapper
public interface EmpMapper {


    /**
     * 批量删除员工
     * @param ids
     */
    void deleteBatch(List<Integer> ids);

    /**
     * 添加员工信息
     * @param emp
     */
    void insert(Emp emp);

    /**
     * 根据id查询员工详情
     * @param id
     * @return
     */
    @Select("select * from emp where id = #{id}")
    Emp queryOne(Integer id);


    /**
     *  更新员工表员工信息
     */
    void update(Emp emp);


    /**
     * 根据用户名称和密码查询员工信息
     * @param emp
     * @return
     */
    @Select("select * from emp where username=#{username}")
    Emp selectByNameAndPwd(Emp emp);

   List <Emp> select(PageQueryBean pageBean);

    /**
     * 删除指定部门的所有员工
     * @param id
     */
    @Delete("delete from emp where  dept_id = #{id}")
    void deleteByDeptId(Integer id);
}











