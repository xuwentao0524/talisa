package com.itheima.mapper;

import com.itheima.entity.Dept;
import com.itheima.entity.Emp;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 定义了所有部门相关的持久层方法
 */
@Mapper
public interface DeptMapper {

    //查询所有部门列表
    @Select("select * from dept")
    List<Dept> selectAll();

    //根据id删除部门信息
    @Delete("delete from dept  where  id  =  #{id}")
    void deleteById(Integer id);

    //添加部门信息
    @Insert("insert into dept (id, name, create_time, update_time) values (null,#{name},#{createTime},#{updateTime});")
    void insert(Dept dept);

    //根据ID查询 数据表dept
    @Select("select * from dept where id = #{id}")
    Dept selectDeptById(Integer id);

    @Update("update dept set id =#{id},name=#{name} where id=#{id}")
    void update(Dept dept);
}
