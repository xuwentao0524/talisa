package com.itheima.filter;

import com.itheima.util.JwtUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录验证过滤器
 */
//@WebFilter("/*")
public class CheckLoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //servletRequest  本质上就是 HttpServletRequest 对象，使用父类接收而已
        HttpServletRequest request = (HttpServletRequest) servletRequest;//(请求数据包中的所有数据封装的对象)
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //1.获取用户请求路径（门卫：询问你进入小区干啥？）
        String requestURI = request.getRequestURL().toString(); //资源完整路径：协议+ip地址+端口号+请求路径
        //String requestURI2 = request.getRequestURI();//资源短路径：请求路径
        System.out.println("当前请求路径："+requestURI);

        //2.判断是否是登录请求（门卫：判断是否是来办理门禁卡）
        if(requestURI.contains("/login")){
            //如果是登录，直接放行（办理门禁卡，直接放行）
            filterChain.doFilter(request,response);
            return; //访问完目标资源后，直接结束过滤操作
        }
        //3. 访问其他的资源（直接想进入小区），验证用户的身份
        String token = request.getHeader("Token");
        //3.1 检查令牌（出示门禁卡）
        if(token==null){
            //没有携带令牌（没有小区的门禁卡），禁止后续访问
            //跳转登录页面（办理门禁的办事处，验证身份办理门禁）
            //手动封装响应结果数据：{"code":0,"msg":"NOT_LOGIN"}
            response.getWriter().write("{\"code\":0,\"msg\":\"NOT_LOGIN\"}");
            return; //注意：
        }
        //3.2 校验jwt令牌是否正确
        try {
            JwtUtils.parseJWT(token);
        } catch (Exception e) {
            e.printStackTrace();
            //如果令牌解析错误（门禁卡无效），禁止后续访问
            //跳转登录页面（办理门禁的办事处，验证身份办理门禁）
            //手动封装响应结果数据：{"code"=0,"msg"="NOT_LOGIN"}
            response.getWriter().write("{\"code\":0,\"msg\":\"NOT_LOGIN\"}");
            return; //注意：
        }
        //4. 携带令牌/令牌正确，放行后续资源访问（进入小区）
        filterChain.doFilter(request,response);
    }
}




















