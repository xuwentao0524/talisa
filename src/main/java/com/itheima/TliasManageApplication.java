package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;


//开启 : Servlet  组件   扫描
@ServletComponentScan

@SpringBootApplication
public class TliasManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(TliasManageApplication.class, args);
    }

}
