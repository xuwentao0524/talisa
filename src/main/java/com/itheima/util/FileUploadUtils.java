package com.itheima.util;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * 文件上传的工具类
 */
@Component
public class FileUploadUtils {

    @Autowired
    AliOSSProperties aliOSSProperties;

    /**
     * 上传文件到阿里oss
     */
    public  String uploadFile(MultipartFile image) throws IOException {
        //1. 获取用户上传文件的名称
        String originalFilename = image.getOriginalFilename(); //fj.jpg     合同.pdf
        //从文件名称中截取出文件的后缀
        String fileType = originalFilename.substring(originalFilename.lastIndexOf("."));
        //2. 生成唯一随机资源名称
        String fileName = UUID.randomUUID().toString() + fileType;
        //3. 调用MultipartFile 存储到oss
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。

        // 3.1创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(aliOSSProperties.getEndpoint(),aliOSSProperties.getAccessId(),aliOSSProperties.getAccsessKey());
        try {
            //3.2 上传文件
            ossClient.putObject(aliOSSProperties.getBucketName(), fileName, image.getInputStream());
        }  catch (ClientException ce) {
            ce.printStackTrace();
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        //4.返回可以直接访问oss的资源路径
        return "https://"+aliOSSProperties.getBucketName()+"."+aliOSSProperties.getEndpoint()+"/"+fileName;
    }

}
