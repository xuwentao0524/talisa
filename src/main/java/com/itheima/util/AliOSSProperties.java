package com.itheima.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 封装配置文件中一组配置信息的实体对象
 */
//自动编写各种get/set/toString... ...
@Data

//ioc容器扫描此注解，实例化此对象
@Component

//ConfigurationProperties 注解：从配置文件application.yml中一次性解析一组指定的配置信息
@ConfigurationProperties(prefix = "aliyun.oss")

public class AliOSSProperties {

   private String endpoint   ;// : oss-cn-beijing.aliyuncs.com
   private String accessId   ;// : LTAI5tEoJkqRcMaTLaSm793M
   private String accsessKey ;// : iu7ptvg5giuhFhTbJ8qm8KkHKqG8SY
   private String bucketName ;// : tlias-kunkun-manage

}
