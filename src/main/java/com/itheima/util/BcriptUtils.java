package com.itheima.util;


import org.mindrot.jbcrypt.BCrypt;

/**
 * Bcript加盐加密工具类
 */
public class BcriptUtils {

    // 生成盐并加密密码
    public static String hashPassword(String password) {
        String salt = BCrypt.gensalt();
        return BCrypt.hashpw(password, salt);
    }

    // 验证密码
    public static boolean verifyPassword(String password, String hashedPassword) {
        return BCrypt.checkpw(password, hashedPassword);
    }

    public static void main(String[] args) {
        /*String password = "kunkun123";

        // 加密密码
        String hashedPassword = hashPassword(password);
        System.out.println("Hashed Password: " + hashedPassword);*/

        //$2a$10$gQQuRqUwo./CWmXJD2cKQe8D8iaQMURSMzxPe.Ls//Y/kiYVarl/O
        //$2a$10$EfHuyiaZU5anAKvHqWlJQ.20SDosbdxpUKref7MSCldq05cbGRg3y
        //$2a$10$SR2HycA2OlmuYBQ8if9NF.K3arycq.w5QcHU0vAqHcPNnZfOqiokC


        // 验证密码
        String inputPassword = "kunkun123";
        if (verifyPassword(inputPassword, "$2a$10$EfHuyiaZU5anAKvHqWlJQ.20SDosbdxpUKref7MSCldq05cbGRg3y")) {
            System.out.println("Password is correct!");
        } else {
            System.out.println("Password is incorrect!");
        }
    }
}
