package com.itheima.service;

import com.itheima.entity.Dept;
import com.itheima.entity.Emp;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 定义部门相关的所有功能接口方法
 */
public interface DeptService {

    //查询部门列表的业务方法
    List<Dept> listDept();

    //根据指定id删除部门业务方法
    void removeById(Integer id) throws ClassNotFoundException;

    //添加部门业务方法
    void saveDept(Dept dept);

    //根据ID查询部门 业务方法
    Dept queryDeptById(Integer id);


    void update(Dept dept);
}

