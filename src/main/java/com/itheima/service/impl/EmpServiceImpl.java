package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.entity.Emp;
import com.itheima.entity.PageBean;
import com.itheima.entity.PageQueryBean;
import com.itheima.mapper.EmpMapper;
import com.itheima.service.DeptService;
import com.itheima.service.EmpService;
import com.itheima.util.BcriptUtils;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 实现员工相关的所有业务功能方法
 */
@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    EmpMapper empMapper;


    //批量删除员工业务方法
    @Override
    public void deleteBatch(List<Integer> ids) {
        //1. 调用持久层，批量根据id删除员工信息
        empMapper.deleteBatch(ids);
    }

    //添加员工信息业务方法
    @Override
    public void addEmp(Emp emp) {
        //1.补充基础属性
        emp.setCreateTime(LocalDateTime.now());
        emp.setUpdateTime(LocalDateTime.now());

        /**
         * 设置密码：加密存储
         * 加密的方式一 MD5，DigestUtils.md5DigestAsHex()
         * 加密的方式二 Bcript加盐加密，密文不可逆，可以验证，安全性高
         */
        emp.setPassword(BcriptUtils.hashPassword("123456"));


        //2.调用持久层mapper添加员工信息
        empMapper.insert(emp);
    }

    //查询员工详情信息业务方法
    @Override
    public Emp queryOne(Integer id) {
        Emp emp = empMapper.queryOne(id);
        return emp;
    }

    @Override
    public void update(Emp emp) {
        //补充基础属性
        emp.setUpdateTime(LocalDateTime.now());
        //调用持久层更新数据
        empMapper.update(emp);
    }

    //员工登录验证业务方法
    @Override
    public Emp checkLogin(Emp emp) {
        //查询emp表，检查用户信息是否存在
        //1. 查询用户名是否存在：select * from emp  where username = #{username}
        Emp empResult = empMapper.selectByNameAndPwd(emp);
        if(empResult == null){
            //登录失败，用户名不存在
            return null;
        }
        //2. 查询校验密码是否正确（密码加密存储）： asdasdasdsad  ==  123456
                                        // 明文：123456   //密文：加盐加密后的密文
        if(!BcriptUtils.verifyPassword(emp.getPassword(),empResult.getPassword())){
            //登录失败，密码错误
            return  null;
        }

        return empResult;
    }

    @Override
    public PageBean select(PageQueryBean pageQueryBean) {

        PageHelper.startPage(pageQueryBean.getPage(),pageQueryBean.getPageSize()) ;

        Page page = (Page) empMapper.select(pageQueryBean);

        PageBean pageBean = new PageBean();
        pageBean.setTotal((int) page.getTotal());
        pageBean.setRows(page.getResult());



        return pageBean;
    }


}
