package com.itheima.service.impl;

import com.itheima.annotation.MyLog;
import com.itheima.entity.Dept;
import com.itheima.entity.DeptLog;
import com.itheima.mapper.DeptMapper;
import com.itheima.mapper.EmpMapper;
import com.itheima.service.DeptLogService;
import com.itheima.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 实现部门相关的所有业务功能方法
 */
@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    DeptMapper deptMapper;

    @Autowired
    EmpMapper empMapper;


    @Autowired
    DeptLogService deptLogService;

    //查询所有部门列表业务功能实现
    @Override
    public List<Dept> listDept() {

        //1. 调用持久层mapper，查询数据表dept中的所有部门列表数据
        List<Dept> deptList = deptMapper.selectAll();
        //2. 返回查询结果
        return deptList;
    }

    //根据部门id删除部门信息
    //使用框架spring提供的事务管理：申明式事务管理
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void removeById(Integer id) throws ClassNotFoundException {



        try {
            //1. 调用持久层mapper，删除数据表dept中指定的部门信息
            deptMapper.deleteById(id);

            //模拟异常
            //int i = 1/0;  //算数异常，运行时异常
            if (true) {
                throw new ClassNotFoundException(); //类型转换异常，编译器异常
            }

            //2.开除当前部门中的所有员工：delete  from  emp   where  dept_id = ?
            empMapper.deleteByDeptId(id);
        } finally {
            //3.记录日志
            DeptLog deptLog = new DeptLog();
            deptLog.setCreateTime(LocalDateTime.now());
            deptLog.setDescription("解散部门");
            deptLogService.insert(deptLog);
        }




    }

    //添加部门业务方法
    @Override
    public void saveDept(Dept dept) {
        //1. 补全基础属性：createTime  /updateTime
        dept.setCreateTime(LocalDateTime.now());
        dept.setUpdateTime(LocalDateTime.now());

        //2. 调用持久层mapper，向数据表dept中添加新的部门信息
        deptMapper.insert(dept);
    }


    @MyLog
    //根据ID查询部门 业务方法
    @Override
    public Dept queryDeptById(Integer id) {
        //调用持久层，根据ID查询部门
        return deptMapper.selectDeptById(id);
    }

    @MyLog
    @Override
    public void update(Dept dept) {
        dept.setUpdateTime(LocalDateTime.now());
        dept.setCreateTime(LocalDateTime.now());
        deptMapper.update(dept);
    }


}












