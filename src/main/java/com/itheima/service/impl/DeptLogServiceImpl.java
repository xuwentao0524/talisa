package com.itheima.service.impl;

import com.itheima.entity.DeptLog;
import com.itheima.mapper.DeptLogMapper;
import com.itheima.service.DeptLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DeptLogServiceImpl implements DeptLogService {

    @Autowired
    private DeptLogMapper deptLogMapper;

//    @Transactional(propagation = Propagation.REQUIRED) //有则加入，无则单开
    @Transactional(propagation = Propagation.REQUIRES_NEW) //有无都不加入，自己单开事务
    @Override
    public void insert(DeptLog deptLog) {
        deptLogMapper.insert(deptLog);
    }
}
