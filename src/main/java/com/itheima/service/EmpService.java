package com.itheima.service;

import com.itheima.entity.Emp;
import com.itheima.entity.PageBean;
import com.itheima.entity.PageQueryBean;

import java.util.List;
import java.util.Map;

/**
 * 定义员工相关的所有功能接口方法
 */
public interface EmpService {


    /**
     * 批量删除员工业务方法
     * @param ids
     */
    void deleteBatch(List<Integer> ids);

    /**
     * 添加员工业务方法
     * @param emp
     */
    void addEmp(Emp emp);

    /**
     * 查询员工详情信息业务方法
     * @param id
     * @return
     */
    Emp queryOne(Integer id);


    void update(Emp emp);

    /**
     * 员工登录业务方法
     * @param emp
     * @return
     */
    Emp checkLogin(Emp emp);

     PageBean select(PageQueryBean pageQueryBean);
}
