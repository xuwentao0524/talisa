package com.itheima;

import com.itheima.util.JwtUtils;
import io.jsonwebtoken.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class TliasManageApplicationTests {

    @Test
    void createJwtTokenTest() {
        //创建载荷：存储用户自定义的信息（不建议存储私密的信息）
        Map claimsMap  = new HashMap<>();
        claimsMap.put("userId",123456);
        claimsMap.put("username","kunkun");

        String jwtToken = JwtUtils.generateJwt(claimsMap);
        System.out.println(jwtToken);
    }

    @Test
    void parseJwtTokenTest() {
        Claims claims =
                JwtUtils.parseJWT("eyJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjEyMzQ1NiwidXNlcm5hbWUiOiJrdW5rdW4iLCJleHAiOjE3MDA4MzkwNDR9.Wx3K5lZApf8pSTqlfobL2C5hpriuhQRHul7PKRQv3DE");
        System.out.println(claims);
    }

}
